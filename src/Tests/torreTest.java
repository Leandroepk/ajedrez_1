package Tests;

import Piezas.torre;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.Assert.assertEquals;

public class torreTest {

    @Test

    public void TorreT(){
        Map<String, String> data = new HashMap<String, String>();
        data.put("posicion", "0-0");
        data.put("color", "negro");

        torre la_torre= new torre(data);
        assertEquals(la_torre.mover(5,5),"movimiento_rechazado");
        assertEquals(la_torre.mover(0,4),"movimiento_realizado");
    }
}