package Tests;


import Piezas.Tablero;
import Piezas.rey;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class reyTest {
    @Test
    public void Reysito(){
        Map<String, String> data = new HashMap<String, String>();
        data.put("posicion", "2-2");
        data.put("color", "negro");

        rey reysito= new rey(data);
        assertEquals(reysito.mover(5,5),"movimiento_rechazado");
        assertEquals(reysito.mover(1,1),"movimiento_realizado");
    }



}