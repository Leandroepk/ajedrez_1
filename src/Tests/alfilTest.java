package Tests;

import Piezas.alfil;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class alfilTest {
    @Test
    public void Alfilsito(){
        Map<String, String> data = new HashMap<String, String>();
        data.put("posicion", "2-2");
        data.put("color","negro");

        alfil Alfilsito = new alfil(data);
        assertNotEquals(Alfilsito.mover(5,5),"movimiento_rechazado");
        assertEquals(Alfilsito.mover(1,1),"movimiento_realizado");
    }
}