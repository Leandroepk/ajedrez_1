import Piezas.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class main {
    public static void main(String[] args) {
        ArrayList<Map<String, String>> input = new ArrayList<Map<String, String>>();
        Map<String, String> aux = new HashMap<String, String>();

        aux.put("posicion", "4-4");
        aux.put("color", "negro");
        aux.put("pieza", "D");

        input.add(aux);

        Map<String, String> aux2 = new HashMap<String, String>();

        aux2.put("posicion","7-4");
        aux2.put("color","negro");
        aux2.put("pieza","T");
        input.add(aux2);

        Tablero miTablero = new Tablero(true,input);

    }
}