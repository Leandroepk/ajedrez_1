package Piezas;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Map;

abstract public class pieza {
    ArrayList<Integer> posicion = new ArrayList<Integer>();
    String color;
    int iterador = 0;
    boolean reset = false;
    ArrayList<String>  posEnMedio = new ArrayList<String>();

    public String test(){
        return "O";
    }

    public void importar(Map<String, String> args){

        this.posicion = parsePos(args.get("posicion"));

        this.color = args.get("color");
    }

    public ArrayList<Integer> parsePos(String pos){
        String[] posString = pos.split("-");
        ArrayList<Integer> output = new ArrayList<Integer>();

        int aux = 0;
        for (int i=0;i<posString.length;i++){
            aux = Integer.parseInt(posString[i]);
            output.add(aux);
        }
        return (output);
    }


    public String mover(int x,int y){
        return "";
    }

    /////////////////////////////////////////////////////////////////////////////
    //////////////////METODOS RELACIONADOS CON EL MOVIMIENTO/////////////////////
    /////////////////////////////////////////////////////////////////////////////

    public boolean necesitoMasDatos(int posfinX,int posfinY ){
        if(this.posEnMedio.size()>=this.iterador+1){
            return true;
        }
        else{
            if(reset==false){
                this.posicion.set(0,posfinX);
                this.posicion.set(1,posfinY);
                return false;
            }else{
                reset = false;
                return false;
            }
        }
    }
    public String[] devolverDato(){
        return this.posEnMedio.get(iterador).split("-");
    }
    public void continuarDato(){
        this.iterador++;
    }
    public void borrarDato(){
        this.reset = true;
        this.posEnMedio.clear();
        this.posEnMedio.add("para");
        this.iterador = 2;
    }

}