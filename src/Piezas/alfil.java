package Piezas;


import java.util.Map;

public class alfil extends pieza {
    public alfil(Map<String, String> args){
        importar(args);
    }
    //Math.abs(f1 – f2) == Math.abs(c1 – c2)
    public String mover(int x,int y){
        int deltaX = Math.abs(x-this.posicion.get(0));
        int deltaY = Math.abs(y-this.posicion.get(1));
        if(deltaX==deltaY) {
            return "movimiento_realizado";
        }
        return "movimiento_rechazado";
    }
}
