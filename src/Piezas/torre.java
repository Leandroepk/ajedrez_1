package Piezas;
import java.util.ArrayList;
import java.util.Map;
public class torre extends pieza{
    public torre(Map<String, String> args){importar(args);}

    public String mover(int x,int y){
        if((x==this.posicion.get(0))||(y==this.posicion.get(1))){
            //Para ABAJO
            //Aclaracion: El X que se recibe de mover() es la coordenada Y del tablero
            if (x>this.posicion.get(0)){
                for(int i= this.posicion.get(0)+1;i<=x;i++){
                    String coords = i+"-"+y;
                    this.posEnMedio.add(coords);
                }

                return "mover";
            }
            //Para ARRIBA
            if (x<this.posicion.get(0)){
                for(int i= this.posicion.get(0)-1;i>=x;i--){
                    String coords = i+"-"+y;
                    this.posEnMedio.add(coords);
                }
                return "mover";
            }

            //Para DERECHA
            if (y>this.posicion.get(1)){
                for(int i= this.posicion.get(1)+1;i<=y;i++){
                    String coords = x+"-"+i;
                    this.posEnMedio.add(coords);
                }
                return "mover";
            }

            //Para IZQUIERDA
            if (y<this.posicion.get(1)){
                for(int i= this.posicion.get(1)-1;i>=y;i--){
                    String coords = x+"-"+i;
                    this.posEnMedio.add(coords);
                }
                return "mover";
            }
            return "enroque";
        }else{
            return "movimiento_rechazado";
        }
    }
}