package Piezas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Tablero {
    private ArrayList<ArrayList<Object>> tablero = new ArrayList<ArrayList<Object>>();

    public Tablero(boolean test,ArrayList<Map<String, String>> input){
        if(!test){
            crearTablero();
        }else{
            crearTableroPruebas(input);
        }
        pintar();
        escuchar();
    }

    public void crearTablero(){
        ArrayList<Object> aux = new ArrayList<Object>();
        Map<String, String> data = new HashMap<String, String>();

        for (int i=0;i<8;i++){
            for (int j=0;j<8;j++){
                data.put("posicion" , i+"-"+j);
                if (i<2){
                    data.put("color" , "blanco");
                }else if (i>5){
                    data.put("color" , "negro");
                }

                if (i==1||i==6){
                    aux.add(new peon(data));
                }
                else if (i==0||i==7){
                    if(j==0||j==7){
                        aux.add(new torre(data));
                    }else if(j==1||j==6){
                        aux.add(new caballo(data));
                    }else if(j==2||j==5){
                        aux.add(new alfil(data));
                    }else if(j==3){
                        aux.add(new reina(data));
                    }else if(j==4){
                        aux.add(new rey(data));
                    }
                }else{
                    aux.add(null);
                }
            }
            tablero.add((ArrayList<Object>) aux.clone());
            aux.clear();
        }
    }

    public void crearTableroPruebas(ArrayList<Map<String, String>> input){ //EN PRUEBAS
        ArrayList<Object> aux = new ArrayList<Object>();

        for (int i=0;i<8;i++){
            for (int j=0;j<8;j++){
                aux.add(null);
            }
            tablero.add((ArrayList<Object>) aux.clone());
            aux.clear();
        }
        ArrayList<Integer> pos = new ArrayList<Integer>();
        for (Map<String, String> i : input){
            pos = parsePos(i.get("posicion"));
            switch (i.get("pieza")){
                case "p":
                    tablero.get(pos.get(0)).set(pos.get(1),new peon(i));
                    break;
                case "T":
                    tablero.get(pos.get(0)).set(pos.get(1),new torre(i));
                    break;
                case "A":
                    tablero.get(pos.get(0)).set(pos.get(1),new alfil(i));
                    break;
                case "C":
                    tablero.get(pos.get(0)).set(pos.get(1),new caballo(i));
                    break;
                case "R":
                    tablero.get(pos.get(0)).set(pos.get(1),new rey(i));
                    break;
                case "D":
                    tablero.get(pos.get(0)).set(pos.get(1),new reina(i));
                    break;

            }

        }
    }

    public void pintar(){
        int[] pos = new int[2];

        for (ArrayList<Object> i : tablero){
            for (Object j : i){
                if (j!=null){
                    if (peon.class.equals(j.getClass())) {
                        System.out.print("p\t");
                    } else if (torre.class.equals(j.getClass())) {
                        System.out.print("T\t");
                    } else if (caballo.class.equals(j.getClass())) {
                        System.out.print("C\t");
                    } else if (alfil.class.equals(j.getClass())) {
                        System.out.print("A\t");
                    } else if (reina.class.equals(j.getClass())) {
                        System.out.print("D\t");
                    } else if (rey.class.equals(j.getClass())) {
                        System.out.print("R\t");
                    } else{
                        System.out.print("V\t");
                    }
                }
                else{
                    System.out.print("V\t");
                }
            }
            System.out.println();
        }
    }

    public void escuchar(){
        Scanner sc= new Scanner(System.in);
        String input;
        String[] instrucciones;
        int[] posInicial;
        int[] posFinal;

        int[] parcialPos;

        while(true){
            input = sc.next();
            instrucciones = input.split("->");
            posInicial = parse(instrucciones[1].split("-"));
            posFinal = parse(instrucciones[2].split("-"));

            String result;

            if(comprobar(instrucciones[0],posInicial)){
                if( tablero.get(posInicial[0]).get(posInicial[1]).getClass()==peon.class||
                    tablero.get(posInicial[0]).get(posInicial[1]).getClass()==torre.class||
                    tablero.get(posInicial[0]).get(posInicial[1]).getClass()==caballo.class||
                    tablero.get(posInicial[0]).get(posInicial[1]).getClass()==alfil.class||
                    tablero.get(posInicial[0]).get(posInicial[1]).getClass()==reina.class||
                    tablero.get(posInicial[0]).get(posInicial[1]).getClass()==rey.class)
                {
                    result = ((pieza) tablero.get(posInicial[0]).get(posInicial[1])).mover(posFinal[0],posFinal[1]);
                    boolean ban = true;
                    switch (result){
                        case "mover":
                            while(((pieza) tablero.get(posInicial[0]).get(posInicial[1])).necesitoMasDatos(posFinal[0],posFinal[1])){
                                parcialPos = parse(((pieza) tablero.get(posInicial[0]).get(posInicial[1])).devolverDato());
                                if (tablero.get(parcialPos[0]).get(parcialPos[1])==null){
                                    ((pieza) tablero.get(posInicial[0]).get(posInicial[1])).continuarDato();
                                }
                                else{
                                    ban = false;
                                    ((pieza) tablero.get(posInicial[0]).get(posInicial[1])).borrarDato();
                                    System.out.println("Hay una pieza tuya en medio");
                                }
                            }
                            if(ban){
                                tablero.get(posFinal[0]).set(posFinal[1],tablero.get(posInicial[0]).get(posInicial[1]));
                                tablero.get(posInicial[0]).set(posInicial[1],null);
                            }
                            break;
                        case "enroque":
                            break;
                        case "movimiento_realizado":
                            tablero.get(posFinal[0]).set(posFinal[1],tablero.get(posInicial[0]).get(posInicial[1]));
                            tablero.get(posInicial[0]).set(posInicial[1],null);
                            break;
                        case "movimiento_rechazado":
                            System.out.println("Movimiento invalido");
                            break;
                        default:
                            break;
                    }
                }
            }
            pintar();
        }
    }

    public int[] parse(String[] input){
        int[] output = new int[input.length];
        for (int i=0;i<input.length;i++){
            output[i] = Integer.parseInt(input[i]);
        }
        return output;
    }

    public boolean comprobar(String pieza,int[] pos){
        switch (pieza){
            case "p":
                if(tablero.get(pos[0]).get(pos[1]).getClass()==peon.class){
                    return true;
                }
                break;
            case "T":
                if(tablero.get(pos[0]).get(pos[1]).getClass()==torre.class){
                    return true;
                }
                break;
            case "C":
                if(tablero.get(pos[0]).get(pos[1]).getClass()==caballo.class){
                    return true;
                }
                break;
            case "A":
                if(tablero.get(pos[0]).get(pos[1]).getClass()==alfil.class){
                    return true;
                }
                break;
            case "D":
                if(tablero.get(pos[0]).get(pos[1]).getClass()==reina.class){
                    return true;
                }
                break;
            case "R":
                if(tablero.get(pos[0]).get(pos[1]).getClass()==rey.class){
                    return true;
                }
                break;
        }
        return false;
    }

    public ArrayList<Integer> parsePos(String pos){
        String[] posString = pos.split("-");
        ArrayList<Integer> output = new ArrayList<Integer>();

        int aux = 0;
        for (int i=0;i<posString.length;i++){
            aux = Integer.parseInt(posString[i]);
            output.add(aux);
        }
        return (output);
    }
}
